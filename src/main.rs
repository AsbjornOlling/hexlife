use yew::prelude::*;
use std::f32::consts::PI;
use itertools::Itertools;

enum Msg {
    Step,
    Toggle(usize, usize)
}

const GRIDSIZE: usize = 64;
type Grid = [[bool;GRIDSIZE];GRIDSIZE];

struct Model {
    grid: Grid
}

fn prev_modulo(n: usize) -> usize {
    // janky manual modulo on n-1
    if n == 0 {GRIDSIZE-1} else {n-1}
}

fn next_modulo(n: usize) -> usize {
    // janky manual modulo on n+1
    if n == GRIDSIZE-1 {0} else {n+1}
}

fn alive_neighbors(coords: (usize, usize), grid: Grid) -> usize {
    // finds how many neighbors are alive
    let x = coords.0;
    let y = coords.1;
    let x_prev = prev_modulo(x);
    let x_next = next_modulo(x);
    let y_prev = prev_modulo(y);  
    let y_next = next_modulo(y);
    let neighbors: [usize; 6] =  
        if x%2 == 1 {[
        grid[x_prev][y] as usize,
        grid[x][y_prev] as usize,
        grid[x_next][y] as usize,
        grid[x_prev][y_next] as usize,
        grid[x][y_next] as usize,
        grid[x_next][y_next] as usize,
        ]
        }
        else {[
        grid[x_prev][y_prev] as usize,
        grid[x][y_prev] as usize,
        grid[x_next][y_prev] as usize,
        grid[x_prev][y] as usize,
        grid[x][y_next] as usize,
        grid[x_next][y] as usize,
        ]
        }
    ;
    println!("{:?}", neighbors);
    
    let sum = neighbors.iter().sum();
    println!("{:?}", sum);
    sum
}

fn update(coords: (usize, usize), grid: Grid) -> bool {
    // produces the updated value at a coordinate
    if [2].contains(&alive_neighbors(coords, grid)) {
        true
    } else {
        false
    }
}

fn step(now: Grid) -> Grid {
    // updates all grid cells
    let mut next: Grid = [[false; GRIDSIZE]; GRIDSIZE];
    for x in 0..(GRIDSIZE) {
        for y in 0..(GRIDSIZE) {
            next[x][y] = update((x,y), now)
        }
    };
    next
}

fn svg_hexagon(cx: f32, cy: f32, r: f32, alive: bool, callback: Callback<MouseEvent>) -> Html {
    // make an svg hexagon centered in `(cx, cy)` with radius `r`
    let corners: [(f32, f32); 6] = [
        (cx + r, cy),
        (cx + r * (PI / 3.).cos(),
         cy + r * (PI / 3.).sin()),
        (cx + r * (2. * PI / 3.).cos(),
         cy + r * (2. * PI / 3.).sin()),
        (cx + r * (3. * PI / 3.).cos(),
         cy + r * (3. * PI / 3.).sin()),
        (cx + r * (4. * PI / 3.).cos(),
         cy + r * (4. * PI / 3.).sin()),
        (cx + r * (5. * PI / 3.).cos(),
         cy + r * (5. * PI / 3.).sin()),
    ];
    let svg_points: String = corners.map(|(x, y)| format!("{},{}", x, y)).join(" ");
    html!{
        <polygon
            points={svg_points}
            stroke="#E8A11A"
            fill={ if alive { "#E8301A" } else { "#DB6D23" } }
            stroke-width="2"
            onclick={callback}
        />
    }
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            // init empty grid
            grid: [[false;GRIDSIZE];GRIDSIZE],
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg:Self::Message) -> bool {
        match msg {
            Msg::Step => {
                self.grid = step(self.grid);
                // the value has changed so we need to
                // re-render for it to appear on the page
                true
            }

            Msg::Toggle(x,y) => {
                self.grid[x][y] = !self.grid[x][y];
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        // this gives us a component's `Scope` which allows us to send messages, etc to the component.
        let link = ctx.link();

        // render hex grid
        let radius: f32 = 10.;
        let offset = radius;
        let w = (3 as f32).sqrt() * radius;
        let h = 2. * radius;
        let x_diff = (3./4.) * h;
        let y_diff = w;
        let hex_centers = (0..GRIDSIZE)
            .cartesian_product(0..GRIDSIZE)
            .map(|(x,y)|
                    (x,
                     y,
                     offset + (x as f32 * x_diff),
                     offset + (y as f32 * y_diff) + (if x % 2 == 0 { 0. } else { w / 2. }),
                     self.grid[x as usize][y as usize]));

        // make the html elements from a list of centers and alive status
        let hexagons: Vec<Html> = hex_centers.map(|(x, y, px, py, alive)|
            svg_hexagon(px, py, radius, alive, link.callback(move |_| Msg::Toggle(x, y)))
        ).collect();

        html! {
            <div>
                <button onclick={link.callback(|_| Msg::Step)}>{ "step" }</button>
                <center>
                    <svg height="1000" width="1000">
                        { hexagons }
                    </svg>
                </center>
            </div>
        }
    }
}

fn main() {
    yew::start_app::<Model>();
}
